package iut_gate;


import dataModel.DB_UserData;
import dataModel.GateStructure;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Student;
import service.*;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;


public class Main extends Application {

    DB_Conn conn = new DB_Conn();





    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("iut_gate.fxml"));
        GridPane root = (GridPane) loader.load();
        Controller controller = loader.<Controller>getController();

        primaryStage.setTitle("IUT Smart Gate");
        primaryStage.setScene(new Scene(root, 1200, 760));
        primaryStage.show();


        conn.DbConnect();



        Smart_Card_Svc scs = new Smart_Card_Svc();


        Hardware_Svc hardware = new Hardware_Svc();
        try {
            hardware.initHardware();
            scs.doAuthenticate(hardware, conn, controller);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }



    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void stop(){
        System.out.println("Stage is closing");

        conn.DbConnectClose();
        System.exit(0);
        // Save file
    }
}
