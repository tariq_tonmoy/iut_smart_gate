package iut_gate;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Controller {

    public interface controllerEventListener{
        void swapGateDirection();
        void swapGreetings();
    }

    private List<controllerEventListener> listeners =new ArrayList<controllerEventListener>();

    public void addListener(controllerEventListener listener){
        listeners.add(listener);
    }

    @FXML
    private Label nameTxt, idTxt, countryTxt, greetTxt;

    @FXML
    private ImageView studentImage;

    @FXML
    public void changeName(String name) {
        nameTxt.setText(name);
    }

    @FXML
    public void changeImg(String url) {
        if (url == null)
            url = "IUT-logo.jpg";
        studentImage.setImage(new Image(url));
    }

    @FXML
    public void changeId(String id) {
        idTxt.setText(id);
    }

    @FXML
    public void changeCountry(String country) {
        countryTxt.setText(country);
    }

    @FXML
    public void changeGreet(String greet) {
        greetTxt.setText(greet);
    }

    @FXML
    public void closeProgram() {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    public void swapDirection() {
        for(controllerEventListener listener: listeners){
            listener.swapGateDirection();
        }
    }

    @FXML
    public  void swapGreetings(){
        for(controllerEventListener listener: listeners){
            listener.swapGreetings();
        }
    }



}
