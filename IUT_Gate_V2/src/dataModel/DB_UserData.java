package dataModel;

import model.Student;
import service.DB_Conn;

import java.sql.ResultSet;

/**
 * Created by Tariq on 7/29/2017.
 */

public class DB_UserData {
    DB_Conn conn = null;
    String imageUrl = "http://10.220.20.6/image/";

    public DB_UserData(DB_Conn conn) {
        this.conn = conn;
    }

    public Student GetDataByID(String id, Student s) {

        String queryStr = "SELECT * FROM student_profile WHERE studentid='" + id + "'";
        if (conn != null) {
            try {
                if(conn.IsDbClosed()){
                    conn.DbConnect();
                }
                ResultSet rs = conn.execQuery(queryStr);
                while (rs.next()) {
                    s.setStId(id);
                    s.setStName(rs.getString("name"));
                    s.setCountry(rs.getString("country"));
                    s.setImageUrl(imageUrl + rs.getString("picture"));

                    return s;
                }

            } catch (Exception e) {
                conn.DbConnectClose();
            }
        }
        return null;
    }


}
