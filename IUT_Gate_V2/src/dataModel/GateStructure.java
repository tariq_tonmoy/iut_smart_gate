package dataModel;

import javax.smartcardio.CardTerminal;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Tariq on 7/28/2017.
 */
public class GateStructure {
    int num_gates = 0;
    int portNum = 3;
    private static final String comPort = "COM";
    List<Gate> gates;
    private String currentIDIn;

    public String getCurrentIDIn() {
        return currentIDIn;
    }

    public void setCurrentIDIn(String currentIDIn) {
        this.currentIDIn = currentIDIn;
    }

    private String currentIDOut;

    public String getCurrentIDOut() {
        return currentIDOut;
    }

    public void setCurrentIDOut(String currentIDOut) {
        this.currentIDOut = currentIDOut;
    }

    private String in_terminal_greet = "Welcome", out_terminal_greet = "Goodbye";
    private boolean swappedGreet = true;
    private Path p;

    public class Gate {
        CardTerminal in_terminal, out_terminal;
        String portName;

        public Gate(CardTerminal in_terminal, CardTerminal out_terminal, String portName) {
            this.in_terminal = in_terminal;
            this.out_terminal = out_terminal;
            this.portName = portName;
        }

        public void swapDirection() {
            CardTerminal temp = this.in_terminal;
            this.in_terminal = this.out_terminal;
            this.out_terminal = temp;
        }
    }

    public GateStructure(int num_gates) {
        this.num_gates = num_gates;
        gates = new ArrayList<>();
        p = Paths.get("greet.txt");
        try {
            if (Files.exists(p)) {
                List<String> lines = Files.readAllLines(p);
                if (lines.get(0).equals("1")) {
                    this.in_terminal_greet = "Welcome";
                    this.out_terminal_greet = "Goodbye";
                } else {
                    this.out_terminal_greet = "Welcome";
                    this.in_terminal_greet = "Goodbye";
                }
            }
        } catch (IOException e) {
            System.exit(0);
        }
    }

    public Gate AddGate(CardTerminal in_terminal, CardTerminal out_terminal) {


        gates.add(new Gate(in_terminal, out_terminal, comPort + portNum));

        portNum++;

        return gates.get(gates.size() - 1);

    }

    public boolean is_in_terminal(CardTerminal terminal) {
        for (Gate gate : gates) {
            if (gate.in_terminal.equals(terminal))
                return true;
            else if (gate.out_terminal.equals(terminal))
                return false;
        }
        return false;
    }

    public String get_terminal_greeting(CardTerminal terminal) {
        for (Gate gate : gates) {
            if (gate.in_terminal.equals(terminal))
                return this.in_terminal_greet;
            else if (gate.out_terminal.equals(terminal))
                return this.out_terminal_greet;
        }
        return "";
    }


    public void swapTerminals(Gate gate) {
        Path p = Paths.get("gateDir.txt");
        swapGateSetup(p);
    }

    private String swapGateSetup(Path p) {
        try {
            if (Files.notExists(p)) {
                Files.createFile(p);
                Files.write(p, Arrays.asList("2", "1"));
                return "2";
            } else {
                List<String> lines = Files.readAllLines(p);
                if (lines.get(0).equals("1")) {
                    lines = Arrays.asList("2", "1");
                } else {
                    lines = Arrays.asList("1", "2");
                }
                Files.write(p, lines);
                return lines.get(0);
            }

        } catch (Exception e) {
            System.exit(0);
            return "";
        }
    }

    public void swapGreetings(Gate gate) {
        Path p = Paths.get("greet.txt");
        String str = swapGateSetup(p);
        if (str.equals("1")) {
            this.in_terminal_greet = "Welcome";
            this.out_terminal_greet = "Goodbye";
        } else {
            this.out_terminal_greet = "Welcome";
            this.in_terminal_greet = "Goodbye";
        }
    }


}