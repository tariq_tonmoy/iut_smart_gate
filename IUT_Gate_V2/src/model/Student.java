package model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Tariq on 7/29/2017.
 */
public class Student extends Thread {


    public StringProperty stNamePropProperty() {
        return stNameProp;
    }


    public StringProperty stIdPropProperty() {
        return stIdProp;
    }


    public StringProperty countryPropProperty() {
        return countryProp;
    }


    public StringProperty imageUrlPropProperty() {
        return imageUrlProp;
    }

    public String getStName() {
        return stName;
    }

    public String getStId() {
        return stId;
    }

    public String getCountry() {
        return country;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    private StringProperty stNameProp;
    private StringProperty stIdProp;
    private StringProperty countryProp;
    private StringProperty imageUrlProp;
    private StringProperty greetProp;


    public String getGreetProp() {
        return greetProp.get();
    }

    public StringProperty greetPropProperty() {
        return greetProp;
    }


    public void setStName(String stName) {
        this.stName = stName;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String stName;
    private String stId;
    private String country;
    private String imageUrl;

    public String getGreet() {
        return greet;
    }

    public void setGreet(String greet) {
        this.greet = greet;
    }

    private String greet;

    public Student() {
        stNameProp = new SimpleStringProperty(this, "String", "");
        stIdProp = new SimpleStringProperty(this, "String", "");
        countryProp = new SimpleStringProperty(this, "String", "");
        imageUrlProp = new SimpleStringProperty(this, "String", "");
        greetProp = new SimpleStringProperty(this, "String", "");

        setDaemon(true);
    }


    @Override
    public void run() {
        while (true) {
            stNameProp.set(getStName());
            stIdProp.set(getStId());
            countryProp.set(getCountry());
            imageUrlProp.set(getImageUrl());
            greetProp.set(getGreet());
        }
    }


//    private StringProperty name = new SimpleStringProperty();
//    public final String getName(){
//        return name.get();
//    }
//
//    public final void setName(String _name){
//        name.set(_name);
//    }
//
//    public StringProperty nameProperty(){
//        return name;
//    }
//
//    private StringProperty ID = new SimpleStringProperty();
//    public final String getID(){
//        return ID.get();
//    }
//
//    public final void setID(String _id){
//        ID.set(_id);
//    }
//
//    public StringProperty idProperty(){
//        return ID;
//    }
//
//    private StringProperty country = new SimpleStringProperty();
//    public final String getCountry(){
//        return country.get();
//    }
//
//    public final void setCountry(String _country){
//        country.set(_country);
//    }
//
//    public StringProperty countryProperty(){
//        return country;
//    }
//
//    private StringProperty imageUrl = new SimpleStringProperty();
//    public final String getImageUrl(){
//        return imageUrl.get();
//    }
//
//    public final void setImageUrl(String _imageUrl){
//        imageUrl.set(_imageUrl);
//    }
//
//    public StringProperty imageUrlProperty(){
//        return imageUrl;
//    }


}
