package service;

import dataModel.DB_UserData;
import dataModel.GateStructure;
import iut_gate.Controller;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import model.Student;

import java.awt.dnd.DragGestureEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;
import javax.smartcardio.*;

/**
 * Created by Tariq on 7/28/2017.
 */
public class Smart_Card_Svc implements Controller.controllerEventListener {
    private int MAXSIZE;
    private List<CardTerminal> cTerminals;
    private int num_gates = 2;
    private GateStructure gateStruct;

    private Hardware_Svc hardware;
    private GateStructure.Gate gate;
    private DB_UserData uData;
    final Student newStudent = new Student();
    Student st = null;
    private Controller controller;
    private final int waitInit = 200, waitSec = 2000;
    private boolean swappedLED = true;

    public Smart_Card_Svc() {
        this.MAXSIZE = 16;

    }


    private void setUp() throws Exception {
        TerminalFactory tFactory = TerminalFactory.getDefault();
        cTerminals = tFactory.terminals().list();

        if (cTerminals.size() != num_gates)
            throw new Exception();

        gateStruct = new GateStructure(num_gates);
        for (int i = 0; i < cTerminals.size(); i += 2)
            gate = gateStruct.AddGate(cTerminals.get(i), cTerminals.get(i + 1));
    }

    private void setStudent(Controller controller) {
        this.newStudent.stNamePropProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        controller.changeName(newValue);
                                    }
                                }
                        );
                    }
                }
        );
        this.newStudent.imageUrlPropProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        controller.changeImg(newValue);

                                    }
                                }
                        );
                    }
                }
        );
        this.newStudent.stIdPropProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        controller.changeId(newValue);
                                    }
                                }
                        );
                    }
                }
        );
        this.newStudent.countryPropProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        controller.changeCountry(newValue);
                                    }
                                }
                        );
                    }
                }
        );
        this.newStudent.greetPropProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        Platform.runLater(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        controller.changeGreet(newValue);
                                    }
                                }
                        );
                    }
                }
        );
    }

    private void handleCard() {
        String name = "", country = "", id = "", greet = "", image = "IUT-logo.jpg";
        boolean changed = false;
        boolean readerFlag = true;
        int count = 0;

        int IN = 1, OUT = 2;
        while (true) {
            newStudent.setStName(name);
            newStudent.setStId(id);
            newStudent.setImageUrl(image);
            newStudent.setGreet(greet);
            newStudent.setCountry(country);

            if (changed) {
                try {
                    Thread.sleep(waitInit);
                    count++;
                } catch (Exception e) {
                }
            }
            if (count > (waitSec - waitInit) / waitInit) {
                changed = false;
                name = "";
                country = "";
                id = "";
                greet = "";
                image = "IUT-logo.jpg";
                count = 0;
                gateStruct.setCurrentIDIn("");
                gateStruct.setCurrentIDOut("");
            }

            if (!readerFlag) {
                try {
                    greet = "Card Reader Not Connected! Try Restarting the Program";
                    name = "";
                    country = "";
                    id = "";
                    image = "IUT-logo.jpg";
                    setUp();
                    changed = false;
                    count = 0;
                    readerFlag = true;
                    greet = "";
                    name = "";
                    country = "";
                    id = "";
                    image = "IUT-logo.jpg";

                } catch (Exception ex) {
                    continue;
                }
            }

            try {
                for (CardTerminal cTerminal : cTerminals) {
                    if (cTerminal.isCardPresent()) {
                        boolean res = isAuthenticated(cTerminal);
                        if (res) {
                            if (swappedLED) {
                                Path p = Paths.get("gateDir.txt");

                                try {
                                    if (Files.exists(p)) {
                                        List<String> lines = Files.readAllLines(p);
                                        if (lines.get(0).equals("1")) {
                                            IN = 1;
                                            OUT = 2;
                                        } else {
                                            IN = 2;
                                            OUT = 1;
                                        }
                                        swappedLED = false;
                                    }
                                } catch (IOException ioex) {
                                    System.exit(0);
                                }
                            }
                            if (gateStruct.is_in_terminal(cTerminal)) {
                                String currentID = gateStruct.getCurrentIDIn();
                                if (st.getStId().equals(currentID))
                                    continue;
                                gateStruct.setCurrentIDIn(st.getStId());
                                hardware.setOutputStream(IN);
                            } else {
                                String currentID = gateStruct.getCurrentIDOut();
                                if (st.getStId().equals(currentID))
                                    continue;
                                gateStruct.setCurrentIDOut(st.getStId());
                                hardware.setOutputStream(OUT);
                            }

                            greet = gateStruct.get_terminal_greeting(cTerminal);
                            name = st.getStName();
                            country = st.getCountry();
                            id = st.getStId();
                            image = st.getImageUrl();
                        } else {
                            greet = "AUTHENTICATION FAILED";
                            name = "";
                            country = "";
                            id = "";
                            image = "IUT-logo.jpg";
                        }

                        changed = true;
                        count = 0;
                    }
                }
                Thread.sleep(waitInit);

            } catch (CardException e) {
                System.out.println("Card Exception is: " + e);

                readerFlag = false;

            } catch (Exception e) {
                System.out.println(e);
            }


        }


    }

    @Override
    public void swapGateDirection() {

        gateStruct.swapTerminals(gate);
        swappedLED = true;
    }

    @Override
    public void swapGreetings() {
        gateStruct.swapGreetings(gate);
    }

    public void doAuthenticate(Hardware_Svc hardware, DB_Conn conn, Controller controller) throws Exception {
        setUp();
        this.hardware = hardware;
        this.uData = new DB_UserData(conn);
        this.controller = controller;
        this.controller.addListener(this);


        setStudent(controller);
        newStudent.start();
        Thread th = new Thread(() -> handleCard());
        th.start();
    }

    private boolean isAuthenticated(CardTerminal cTerminal) throws Exception {
        Card card = cTerminal.connect("T=1");
        CardChannel channel = card.getBasicChannel();

        CommandAPDU cmdAuth = new CommandAPDU(hexStringToByteArray("FF 86 00 00 05 01 00 04 60 00"));
        ResponseAPDU resAuth = channel.transmit(cmdAuth);

        ResponseAPDU read = channel.transmit(new CommandAPDU(hexStringToByteArray("FF CA 00 00 00")));
        ResponseAPDU read2 = channel.transmit(new CommandAPDU(hexStringToByteArray("FF B0 00 04 10")));

        String sid = bytesToHex(read2.getData()).replaceFirst("A+", "");

        String uid = bytesToHex(read.getData());
        System.out.println(uid);
        st = new Student();
        Student t = uData.GetDataByID(sid, st);
        return t == null ? false : true;
    }


    public String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private byte[] hexStringToByteArray(String s) {

        String[] strArr = s.split(" ");
        s = "";
        for (String strArr1 : strArr) {
            s += strArr1;
        }

        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }


}
