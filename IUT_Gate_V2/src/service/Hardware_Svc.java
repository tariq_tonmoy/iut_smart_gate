package service;

import java.io.*;
import java.util.*;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;
/**
 * Created by Tariq on 7/28/2017.
 */
public class Hardware_Svc {

    static Enumeration portList;
    static CommPortIdentifier portId;

    static SerialPort serialPort;
    static OutputStream outputStream;

    private String portName = "COM3";


    public void initHardware() throws IOException
    {
        // TODO code application logic here
        System.setProperty("gnu.io.rxtx.SerialPorts", portName);

        portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements())
        {
            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {

                if (portId.getName().equals(portName))
                {
                    try
                    {
                        serialPort = (SerialPort)
                                portId.open("SimpleWriteApp", 2000);

                    }
                    catch (PortInUseException e) {
                        e.printStackTrace();
                    }

                    outputStream = serialPort.getOutputStream();

                    try
                    {
                        serialPort.setSerialPortParams(9600,
                                SerialPort.DATABITS_8,
                                SerialPort.STOPBITS_1,
                                SerialPort.PARITY_NONE);
                    }
                    catch (UnsupportedCommOperationException e) {
                        e.printStackTrace();

                    }
                }
            }
        }
    }


    public void setOutputStream(int val){
        try
        {
            //Authenticated "in"
            if(val==1){
                outputStream.write("1".getBytes());
            }
            //Authenticated "out"
            else if(val==2){
                outputStream.write("2".getBytes());

            }


        }
        catch (Exception e) {
            System.out.println("Cannot write in Serial");
        }
    }
}
