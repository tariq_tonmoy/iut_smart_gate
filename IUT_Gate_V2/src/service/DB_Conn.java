package service;

import java.sql.*;

/**
 * Created by Tariq on 7/28/2017.
 */
public class DB_Conn {
    Connection conn = null;
    String url = "jdbc:mysql://10.220.20.6:3306/smartiutDev",
            user = "smart",
            pass = "secret";

    public void DbConnect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Connected");
        } catch (Exception e) {
           this.DbConnectClose();

        }
    }

    public boolean IsDbClosed(){
        try{
            return conn.isClosed();
        }catch(Exception e){
            return true;
        }
    }
    public void DbConnectClose() {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception e) {

            }
        }
    }

    public ResultSet execQuery(String query) throws Exception {
        Statement st = conn.createStatement();
        return st.executeQuery(query);
    }

}
