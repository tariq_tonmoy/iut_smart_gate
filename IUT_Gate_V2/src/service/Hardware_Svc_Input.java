package service;

import java.io.*;
import java.util.*;

import gnu.io.*;

/**
 * Created by Tariq on 7/28/2017.
 */
public class Hardware_Svc_Input implements Runnable,SerialPortEventListener{

    static Enumeration portList;
    static CommPortIdentifier portId;

    static SerialPort serialPort;
    static OutputStream outputStream;
    static InputStream inputStream;

    private String portName = "COM3";
    public boolean writeMutex=true;

    Thread readThread;
    public void initHardware() throws IOException
    {
        // TODO code application logic here
        System.setProperty("gnu.io.rxtx.SerialPorts", portName);

        portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements())
        {
            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {

                if (portId.getName().equals(portName))
                {
                    try
                    {
                        serialPort = (SerialPort)
                                portId.open("SimpleWriteApp", 2000);

                    }
                    catch (PortInUseException e) {
                        e.printStackTrace();
                    }

                    outputStream = serialPort.getOutputStream();
                    inputStream=serialPort.getInputStream();
                    try
                    {
                        serialPort.addEventListener(this);
                        serialPort.notifyOnDataAvailable(true);
                        serialPort.setSerialPortParams(9600,
                                SerialPort.DATABITS_8,
                                SerialPort.STOPBITS_1,
                                SerialPort.PARITY_NONE);
                    }
                    catch (UnsupportedCommOperationException e) {
                        e.printStackTrace();

                    }catch(Exception e){

                    }
                    readThread=new Thread(this);
                    readThread.start();
                }
            }
        }
    }


    public void setOutputStream(int val){
        if(!writeMutex)
            return;
        try
        {
            //Authenticated "in"
            if(val==1){
                outputStream.write("1".getBytes());
                writeMutex=false;
            }
            //Authenticated "out"
            else if(val==2) {
                outputStream.write("2".getBytes());
                writeMutex=false;
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        switch(event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:
                byte[] readBuffer = new byte[20];

                try {
                    while (inputStream.available() > 0) {
                        int numBytes = inputStream.read(readBuffer);
                    }
                    System.out.println(new String(readBuffer));
                    writeMutex=true;
                } catch (IOException e) {System.out.println(e);}
                break;
        }
    }

    @Override
    public void run() {

    }
}
