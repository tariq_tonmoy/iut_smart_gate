#define PORT_ONE 8
#define PORT_TWO 11

int timeDelay = 500;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(PORT_ONE, OUTPUT);
  pinMode(PORT_TWO, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  char c;
  
  if (Serial.available())
  {
    c = Serial.read();
    switch (c) {
      case '1':
        digitalWrite(PORT_ONE, HIGH);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(timeDelay);
        digitalWrite(PORT_ONE, LOW);
        digitalWrite(LED_BUILTIN, LOW);
        break;
      case '2':
        digitalWrite(PORT_TWO, HIGH);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(timeDelay);
        digitalWrite(PORT_TWO, LOW);
        digitalWrite(LED_BUILTIN, LOW);
        break;
      default:
        break;
    }

  }

}
