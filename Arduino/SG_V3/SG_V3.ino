#define GATE_A 2
#define GATE_B 4
#define LED_A 5
#define LED_B 6
#define BUZZER 7
#define ACK 8

int ackVal = HIGH;
int totalTime = 5000;
int timeDelay = 100;
int ackDelay = 200;
int buzzerDelay = 200;
int currentTime = 0;
char c;

void MakeAllPinLow() {
  digitalWrite(GATE_A, LOW);
  digitalWrite(LED_A, LOW);
  digitalWrite(GATE_B, LOW);
  digitalWrite(LED_B, LOW);
  digitalWrite(BUZZER, LOW);
  currentTime = 0;
}

void checkAck() {
  ackVal = digitalRead(ACK);
  if (ackVal == LOW) {
    delay(ackDelay);
    MakeAllPinLow();
    Serial.print("PASSED");
    ackVal = HIGH;
  }
}
void setup() {
  Serial.begin(9600);

  pinMode(GATE_A, OUTPUT);
  pinMode(GATE_B, OUTPUT);

  pinMode(LED_A, OUTPUT);
  pinMode(LED_B, OUTPUT);

  pinMode(BUZZER, OUTPUT);

  pinMode(ACK, INPUT);

  pinMode(LED_BUILTIN, OUTPUT);

  MakeAllPinLow();
}


void loop() {
  if (currentTime == 0 && Serial.available()) {
    currentTime += timeDelay;
    c = Serial.read();
    switch (c) {
      case '2':
        digitalWrite(GATE_A, HIGH);
        digitalWrite(LED_A, HIGH);
        digitalWrite(BUZZER, HIGH);

        delay(timeDelay);
        break;
      case '1':
        digitalWrite(GATE_B, HIGH);
        digitalWrite(LED_B, HIGH);
        digitalWrite(BUZZER, HIGH);

        delay(timeDelay);
        break;
    }
  }
  else if (currentTime > 0) {
    checkAck();
    if (currentTime == buzzerDelay) {
      digitalWrite(BUZZER, LOW);
      delay(timeDelay);
      currentTime += timeDelay;
    }
    else if (currentTime >= totalTime) {
      MakeAllPinLow();
      Serial.print("TIMEOUT");
    }
    else {
      delay(timeDelay);
      currentTime += timeDelay;
    }
  }

}
